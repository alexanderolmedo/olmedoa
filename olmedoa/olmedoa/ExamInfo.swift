//
//  ExamInfo.swift
//  olmedoa
//
//  Created by alexolmedo on 5/6/18.
//  Copyright © 2018 Alexander Olmedo. All rights reserved.
//

import Foundation



struct Exam: Decodable {
    let viewTitle:String
    let date:String
    let nextLink:String
}

