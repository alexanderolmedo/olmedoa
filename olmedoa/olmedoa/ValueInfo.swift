//
//  ValueInfo.swift
//  olmedoa
//
//  Created by Alexander Olmedo on 6/6/18.
//  Copyright © 2018 Alexander Olmedo. All rights reserved.
//

import Foundation

struct Value: Decodable {
    let label:String
    let value:Int
}
