//
//  NextScreen.swift
//  olmedoa
//
//  Created by alexolmedo on 5/6/18.
//  Copyright © 2018 Alexander Olmedo. All rights reserved.
//

import UIKit

class NextScreenController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var average: UILabel!
    
    
    var nameStringPassed = ""
    var nextLinkPassed = ""
    var responseValues: [Value]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameLabel.text = nameStringPassed
        
        let url = URL(string: nextLinkPassed)
        let session = URLSession.shared
        let task = session.dataTask(with: url!) { (data, response , error ) in
            
            guard let data = data else {
                print("Error, NO data")
                return
            }
            
            guard let valueInfo = try? JSONDecoder().decode([Value].self, from:data) else {
                print("Error decoding value")
                return
            }
            
            DispatchQueue.main.async {
                self.responseValues = valueInfo
                self.table?.reloadData()
                var sum = 0;
                for element in self.responseValues {
                    sum += element.value
                    print(element.value)
                }

                let average = Float(sum) / Float(self.responseValues.count)
   
                self.average.text = "\(average)"
                
            }
    
            
        }
        task.resume()

        // Do any additional setup after loading the view.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        if responseValues != nil {
            cell.textLabel?.text = "\(responseValues[indexPath.row].label)                                                     \(responseValues[indexPath.row].value)"
        } else {
            cell.textLabel?.text = "None"
        }
        
        return cell
    }
    
}
