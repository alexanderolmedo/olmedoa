//
//  ViewController.swift
//  olmedoa
//
//  Created by alexolmedo on 5/6/18.
//  Copyright © 2018 Alexander Olmedo. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var viewTitle: UILabel!
    
    @IBOutlet weak var date: UILabel!
    
    @IBOutlet weak var textFieldName: UITextField!
    
    var nextLink: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let urlString = "https://api.myjson.com/bins/72936"
        let url = URL(string: urlString)
        let session = URLSession.shared
        let task = session.dataTask(with: url!) { (data, response , error ) in
            
            guard let data = data else {
                print("Error, NO data")
                return
            }
            
            guard let examInfo = try? JSONDecoder().decode(Exam.self, from:data) else {
                print("Error decoding Exam")
                return
            }
            
            DispatchQueue.main.async {
                self.viewTitle.text = "\(examInfo.viewTitle)"
                self.date.text = "\(examInfo.date)"
                self.nextLink = "\(examInfo.nextLink)"
                
            }
            
        }
        task.resume()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "nextScreenSegue" {
            let destination = segue.destination as! NextScreenController
            destination.nameStringPassed = textFieldName.text!
            destination.nextLinkPassed = nextLink
        }
    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        print(nextLink)
        performSegue(withIdentifier: "nextScreenSegue", sender: self)
    }
    

}
